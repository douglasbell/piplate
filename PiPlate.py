#!/usr/bin/env python

#
# PiPlate.py
#
# pip install Adafruit_CharLCD
# pip install adafruit_lcd_plate_menu
# pip install netifaces
#

import argparse
import atexit
import datetime
import logging
import threading
import time
import Adafruit_CharLCD as LCD

from adafruit_lcd_plate_menu import CharMenuDisplay
from PiPlateDisplay import PiPlateDisplay
from PiPlateMenu import PiPlateMenu

# Create and initialize the Pi Plate Display instance
_display = PiPlateDisplay()


def main():
    # Exit handler
    atexit.register(exit_handler)

    # Parse logging args
    parser = argparse.ArgumentParser(description='Start the PiePlate')
    parser.add_argument('-v', '--verbosity', required=False, default="critical",
                        choices=['debug', 'info', 'warn', 'error', 'critical'],
                        help='The log level to use.')
    args = parser.parse_args()

    # Set up logging
    numeric_level = getattr(logging, args.verbosity.upper(), None)
    if not isinstance(numeric_level, int):
        print 'Invalid log level: %s. Defaulting to None' % args.log_level
    logging.basicConfig(level=numeric_level,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%Y-%m%-d %I:%M:%S %p')

    # Create the menu items
    pie_plate_menu = PiPlateMenu()
    menu_nodes = pie_plate_menu.create_menu_nodes()

    # Create and start the thread to manage the backlight
    t = threading.Thread(target=manage_backlight)
    t.daemon = True
    t.start()

    # Initialize the menu and display
    CharMenuDisplay(_display.lcd, menu_nodes).display()


def manage_backlight():
    last_button_press_datetime = datetime.datetime.now()
    buttons = [LCD.SELECT, LCD.LEFT, LCD.UP, LCD.RIGHT, LCD.DOWN]
    backlight_off = False

    while True:
        now = datetime.datetime.now()
        last = now - last_button_press_datetime
        if not backlight_off and last.total_seconds() > _display.background_timeout_in_seconds:
            _display.backlight_off()
            backlight_off = True

        for button in buttons:
            if _display.lcd.is_pressed(button):
                last_button_press_datetime = datetime.datetime.now()
                if backlight_off:
                    _display.backlight_on(_display.current_display_color[0],
                                          _display.current_display_color[1],
                                          _display.current_display_color[2])
                    backlight_off = False

        # Ensure the bg color persists (fixing a bug in the AdaFruit library)
        # _display.set_background_color(_display.current_display_color[0],
        #                       _display.current_display_color[1],
        #                       _display.current_display_color[2])
        time.sleep(.2)


def exit_handler():
    _display.lcd.clear()
    _display.backlight_off()

if __name__ == '__main__':
    main()