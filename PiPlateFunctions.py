#
# PiPlateFunctions
#

import os
import socket
import netifaces
import urllib2
import subprocess

class PiPlateFunctions:

    def start_kismet(self):
        self.start_gpsd()
        os.system('nohup /usr/bin/kismet_server --log-prefix ~/kismet -s -c wlan1 &')

    def stop_kismet(self):
        os.system('killall kismet_server')

    def start_gpsd(self):
        os.system('service gpsd start')

    def stop_gpsd(self):
        os.system('service gpsd stop')

    def get_internal_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 0))
        internal_ip = s.getsockname()[0]
        return internal_ip

    def get_external_ip(self):
        external_ip = urllib2.urlopen('http://ipv4.icanhazip.com/').read()
        return external_ip

    def get_hostname(self):
        hostname = socket.gethostname()
        return hostname

    def get_mac_address(self, interface):
        addresses = netifaces.ifaddresses(interface)[netifaces.AF_LINK]
        mac_address = addresses[0]['addr']
        return mac_address

    def get_uptime(self):
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            m, s = divmod(uptime_seconds, 60)
            h, m = divmod(m, 60)
            uptime_str = '%dh %dm %ds' % (int(h), int(m), int(s))
        return uptime_str

    def get_load_average(self):
        load_average_str = '%.2f %.2f %.2f' % os.getloadavg()
        return load_average_str

    def shutdown(self):
        subprocess.call(['shutdown', '-h', 'now'], shell=True)

    def reboot(self):
        subprocess.call(['reboot', '-h'], shell=True)