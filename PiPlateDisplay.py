#
# PiPlateDisplay
#

import Adafruit_CharLCD as LCD

from adafruit_lcd_plate_menu import MenuNode
from PiPlateFunctions import PiPlateFunctions


class PiPlateDisplay:

    _functions = PiPlateFunctions()

    # Create reference to LCD Plate
    lcd = LCD.Adafruit_CharLCDPlate()

    # Default and current backgrouo color
    current_display_color = [0.0, 1.0, 0.0]

    # Default 30 seconds
    background_timeout_in_seconds = 30

    #  Dictionary of background colors
    colors = {'Red': (1.0, 0.0, 0.0),
                'Yellow': (1.0, 1.0, 0.0),
                'Magenta': (1.0, 0.0, 1.0),
                'White': (1.0, 1.0, 1.0),
                'Green': (0.0, 1.0, 0.0),
                'Cyan': (0.0, 1.0, 1.0),
                'Blue': (0.0, 0.0, 1.0),
              }

    def __init__(self):
        self.lcd.clear()
        self.lcd.set_color(self.current_display_color[0],
                           self.current_display_color[1],
                           self.current_display_color[2])

    def set_message(self, message):
        self.lcd.clear()
        self.lcd.message(message)

    def set_color(self, red, blue, green):
        self.lcd.set_color(red, blue, green)

    def backlight_on(self):
        self.lcd.set_backlight(1)

    def backlight_on(self, red, blue, green):
        self.lcd.set_backlight(1)
        self.lcd.set_color(red, blue, green)

    def backlight_off(self):
        self.lcd.set_backlight(0)

    def clear_display(self):
        self.lcd.clear()

    def set_background_color(self, menu_display, selected_node, argvs):
        self.set_color(argvs[0], argvs[1], argvs[2])
        for i in range(0, 3, 1):
            del self.current_display_color[i]
            self.current_display_color.insert(i, argvs[i])

    def set_background_timeout(self,menu_display, selected_node, *argvs):
        global _background_timeout_in_seconds
        _background_timeout_in_seconds = argvs[0]

    #Internal IP
    def display_internal_ip(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            internal_ip = self._functions.get_internal_ip()
            selected_node.add_node(MenuNode(internal_ip))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # External IP
    def display_external_ip(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            external_ip = self._functions.get_external_ip()
            selected_node.add_node(MenuNode(external_ip[:-1]))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # Hostname
    def display_hostname(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            hostname = self._functions.get_hostname()
            selected_node.add_node(MenuNode(hostname))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # MAC Addresses (by interface)
    def display_mac_address(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            interface = argvs[0]
            mac_address = self._functions.get_mac_address(interface)
            selected_node.add_node(MenuNode(mac_address.upper()))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # Uptime
    def display_server_uptime(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            uptime_str = self._functions.get_uptime()
            selected_node.add_node(MenuNode(uptime_str))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # Load
    def display_server_load(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            load_average_str = self._functions.get_load_average()
            selected_node.add_node(MenuNode(load_average_str))
        except:
            selected_node.add_node(MenuNode('Unknown'))

    # Shutdown
    def shutdown(self, menu_display, selected_node, *argvs):
        if len(selected_node.nodes) > 0:
            del selected_node.nodes[0]
        try:
            self.backlight_off()
            self._functions.shutdown()
        except:
            selected_node.add_node(MenuNode('Error'))

    # Reboot
    def reboot(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            self.backlight_off()
            self._functions.reboot()
        except:
            selected_node.add_node(MenuNode('Error'))

    # Start Kismet
    def start_kismet(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            self._functions.start_kismet()
            selected_node.add_node(MenuNode('Started'))
        except:
            selected_node.add_node(MenuNode('Failed'))


    # Stop Kismet
    def stop_kismet(self, menu_display, selected_node, *argvs):
        try:
            if len(selected_node.nodes) > 0:
                del selected_node.nodes[0]
            self._functions.stop_kismet()
            selected_node.add_node(MenuNode('Stopped'))
        except:
            selected_node.add_node(MenuNode('Failed'))