#
# PiPlateMenu
#

import netifaces

from adafruit_lcd_plate_menu import MenuNode
from PiPlateDisplay import PiPlateDisplay

class PiPlateMenu:

    _display = PiPlateDisplay()

    def create_menu_nodes(self):

        menu_nodes = []

        menu_nodes.append(MenuNode('Hostname', None, self._display.display_hostname))
        menu_nodes.append(MenuNode('Internal IP', None, self._display.display_internal_ip))
        menu_nodes.append(MenuNode('External IP', None, self._display.display_external_ip))

        mac_address_interfaces_node = MenuNode('MAC Addresses')
        for interface in netifaces.interfaces():
            mac_address_interfaces_node.add_node(MenuNode(interface, None, self._display. display_mac_address, interface))
        menu_nodes.append(mac_address_interfaces_node)

        menu_nodes.append(MenuNode('Server Load', None, self._display.display_server_load))
        menu_nodes.append(MenuNode('Uptime', None, self._display.display_server_uptime))

        tools_node = MenuNode('Tools')

        wireless_tools_nodes = MenuNode('Kismet')
        wireless_tools_nodes.add_node(MenuNode('Start Kismet', None, self._display.start_kismet))
        wireless_tools_nodes.add_node(MenuNode('Stop Kismet', None, self._display.stop_kismet))

        tools_node.add_node(wireless_tools_nodes)

        menu_nodes.append(tools_node)

        settings_menu_node = MenuNode('Settings')

        color_menu_node = MenuNode('Change Color')
        for color in self._display.colors.keys():
            color_menu_node.add_node(MenuNode(color, None, self._display.set_background_color, self._display.colors[color]))
        settings_menu_node.add_node(color_menu_node)

        backlight_timeout_menu_node = MenuNode('Dislay Timeout')
        backlight_timeout_menu_node.add_node(MenuNode('15 Seconds', None, self._display.set_background_timeout, 15))
        backlight_timeout_menu_node.add_node(MenuNode('30 Seconds', None, self._display.set_background_timeout, 30))
        backlight_timeout_menu_node.add_node(MenuNode('45 Seconds', None, self._display.set_background_timeout, 45))
        backlight_timeout_menu_node.add_node(MenuNode('60 Seconds', None, self._display.set_background_timeout, 60))
        settings_menu_node.add_node(backlight_timeout_menu_node)

        settings_menu_node.add_node(MenuNode('Reboot', None, self._display.reboot))
        settings_menu_node.add_node(MenuNode('Shutdown', None, self._display.shutdown))

        menu_nodes.append(settings_menu_node)

        return menu_nodes